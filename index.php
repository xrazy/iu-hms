<? /* Hotel Management System: UI & Logic Web App */
  
  include_once "core.php"; // main functions
  
  DB::init(DB_HOST, DB_NAME, DB_USER, DB_PASS);
  DB::exec("SET NAMES 'utf8'; SET CHARACTER SET 'utf8';");
  
  // db injection (not api)
  // if(isset($_REQUEST['xquery'])){
  //   die( DB::exec($_REQUEST['xquery'],true) ? "OK" : "ERR: ".$_REQUEST['xquery'] );
  // }
  
  Page::setRoot(PAGE_ROOT);
  Page::addTitle(SITE_NAME);
  
  ob_start(); // capture echo and print
  Page::insert("main.php");
  $content = ob_get_clean();
  
  Page::render($content);
  
?>
