<?php

  include_once 'model/Employee.php';
  include_once 'model/Feature.php';
  include_once 'model/Hotel.php';
  include_once 'model/Record.php';
  include_once 'model/RecordComment.php';
  include_once 'model/Room.php';
  include_once 'model/User.php';
