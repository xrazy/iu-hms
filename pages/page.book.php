<? Included or die(); // book
  Page::addTitle("Book Hotel Room");
?>
<section class="main">
  <div class="container content-medium bg-light">
  
    <h2><?=Page::title();?></h2>
    
    <? if(User::logined()){
      
        $roomid = input('roomid', 0);
         
        $room = Room::getByID($roomid);
        
        if(!empty($room)){
          
          $book = input('book');
          
          if(!empty($book)){
              
            $receptionist = 0;
            
            $result = Record::create(User::current()['UserID'], $receptionist, $roomid, $book['arrival'], $book['departure']);
          
            if($result > 0){ echo '<p>You have successfully booked room.</p>'; } else { echo '<p>Error while add record.</p>'; }
            
          } else { $hotel = Hotel::getByID($room['HotelID']); ?>
        
            <h3><?=$hotel['Name'];?> <small>[<? while($hotel['StarRank']-->0)echo"★";?>]</small></h3>
            
            <p>Room #<?=$room['RoomNum']?></p>
            
            <p>Price: <?=$room['Price'];?></p>
            
            <form action="<?=Page::url();?>" method="post">
              <input type="text" name="book[arrival]" value="<?=filter($_COOKIE['date_arrival']);?>">
              <input type="text" name="book[departure]" value="<?=filter($_COOKIE['date_departure']);?>">
              <input type="submit" value="Book this Room">
            </form>
        
        <? } ?>
        
        <? } else { ?> <p>Room not found.</p> <? } ?>
      
    <? } else { ?>
    
      <p>You should <a href="?page=auth">log in</a> or <a href="?page=register">register</a> before book hotel room!</p>
      
    <? } ?>
    
    <p><a href="/">Go to main</a></p>
    
  </div>
</section>