<? Included or die(); // reg form ?>
<form class="form" method="post" action="<?=Page::url();?>">
  <input type="hidden" name="page" value="register">
  <div class="form-line">
    <input class="field control" type="email" name="register[email]" value="<?=input('register')['email'];?>" placeholder="E-mail" required>
  </div><div class="form-line">
    <input class="field control" type="password" name="register[password]" value="" placeholder="Password" required>
  </div><div class="form-line">
    <input class="field control" type="text" name="register[name]" value="<?=input('register')['name'];?>" placeholder="Full Name" required>
  </div><div class="form-line">
    <input class="field control" type="phone" name="register[phone]" value="<?=input('register')['phone'];?>" placeholder="Phone">
  </div><div class="form-line">
    <button class="button control" type="submit">Register</button>
  </div>
</form>
