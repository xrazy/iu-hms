<? Included or die(); ?>
<div class="bg-red">
  <div class="container"><p id="countdown">deadline</p></div>
</div>
<?
  $script = '$(document).ready(function() {
    var deadline = new Date(2016, 10, 18, 14, 00, 00);
    var block = $("#countdown");
    function get_left(date){ return Math.floor((date - new Date() ) / (1000)); }
    setInterval(function(){
      var left = get_left(deadline);
      var message = Math.floor(left/3600)+" hours or "+Math.floor(left/60)+" minutes or "+left+" seconds until deadline";
      block.html(message);
    }, (1000));
  });';
  Page::addScriptRaw($script);
?>