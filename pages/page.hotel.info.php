<? Included or die(); // hotel info

  $hotelid = input('id', 0);
  
  if(is_numeric($hotelid) && $hotelid > 0){
    
    Page::addTitle("Hotel ".$hotel['Name']);
    
    $hotel = Hotel::getByID($hotelid);
    
    $condition = !$hotel['Disabled']; // TODO: or if admin or if owner
    
    if($condition) { ?>

<section class="main">
  <div class="container content-medium">
    
    <h2><?=$hotel['Name']?></h2>
    
    <? $address = Hotel::getAddress($hotel['AddressID']); ?>
    
    <p>Address: <?=$address['Country'].", ".$address['Locality'].", ".$address['AddressDetails'];?></p>
    
  </div>
</section>

<? } } ?>
