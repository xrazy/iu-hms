<? Included or die(); ?>
<section class="bg-magic">
  <div class="container content-big text-center">
    <div class="caption-big">Searching for bed for tonight?</div>
    <p>We'll help you find the best one for you.</p>
    <form method="post" action="?page=search">
      <input class="control field" type="text" name="search[city]" value="" placeholder="Enter City">
      <button class="control button" type="submit">Find hotels</button>
    </form>
  </div>
</section>
