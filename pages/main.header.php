<? Included or die(); // page header ?>
<header class="main bg-green">

  <div class="container content-small">
    
    <? Page::insert('block.menu.user.php'); ?>
  
    <div class="logo">
      <a href="."><?=Page::title()?></a>
    </div>
    
  </div>
  
</header>