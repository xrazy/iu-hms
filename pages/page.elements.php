<? Included or die(); // ui elements
  Page::addTitle("UI test");
?>
<section class="main">
  <div class="container content-medium bg-light">
  
    <h1>Main heading</h1>
    <p>Slipmouth; round herring walking catfish Steve fish earthworm eel slimehead tenpounder medaka. Sabertooth flying characin coolie loach, clingfish stoneroller minnow, Atlantic silverside. Port Jackson shark gulf menhaden stream catfish Quillfish sand knifefish Mexican golden trout. Eulachon silverside basslet, southern smelt boarfish grouper sucker, lionfish bowfin, ribbonfish scat, trout ground shark ribbonbearer skate pompano.</p>
    
    <h2>Sub heading</h2>
    <p>Не следует, однако забывать, что постоянный количественный рост и сфера нашей активности влечет за собой процесс внедрения и модернизации дальнейших направлений развития. Повседневная практика показывает, что реализация намеченных плановых заданий позволяет выполнять важные задания по разработке новых предложений. Равным образом укрепление и развитие структуры в значительной степени обуславливает создание системы обучения кадров, соответствует насущным потребностям.</p>
    
    <h3>Sub sub heading</h3>
    <p>Ling cod red snapper bocaccio cat shark wolffish freshwater hatchetfish, ghoul snubnose eel goosefish. Wolffish clingfish coolie loach. Pike eel, tiger shovelnose catfish rudd tench requiem shark shell-ear rough scad slender barracudina? Bream; northern anchovy, devil ray, drum cuckoo wrasse yellowhead jawfish char Pacific saury. Pygmy sunfish waryfish convict cichlid Black prickleback pencil catfish.</p>

    <h4>Helper heading 1 (for tables)</h4>
    <table class="table">
      <thead> <tr> <th>First one</th> <th>Second column</th> <th>Third and last</th> </tr> </thead>
      <tbody>
        <tr> <td>Word</td> <td>108</td> <td>*</td> </tr>
        <tr> <td>*</td> <td>Word</td> <td>108</td> </tr>
        <tr> <td>108</td> <td>*</td> <td>Word</td> </tr>
      </thead>
    </table>
    
    <h5>Unused heading 2 (for lists)</h5>
    <ul>
      <li>List item 1</li>
      <li>List item 2</li>
      <li>List item 3</li>
      <li>List item 4</li>
      <li>List item 5</li>
    </ul>
    
    <h6>Unused heading 3 (for nothing?)</h6>
    
    <form class="form">
      <div class="form-line">
        <input class="control field" type="text" placeholder="Some text">
      </div><div class="form-line">
        <input class="control button" type="button" value="The Button">
      </div><div class="form-line">
        <select class="control field"><option>Ooo la la</option><option>Gee thanx</option></select>
      </div><div class="form-line">
        <input class="control other" type="radio" name="lol"><label>Lol 1</label>
      </div><div class="form-line">
        <input class="control other" type="radio" name="lol"><label>Lol 2</label>
      </div><div class="form-line">
        <input class="control other" type="checkbox" name="lal"><label>Lal</label>
      </div><div class="form-line">
        <textarea class="control field">Some text here...</textarea>
      </div><div class="form-line">
        <input class="control other" type="file">
      </div><div class="form-line">
        <button class="control button">BUTTON</button>
      </div>
    </form>
    
  </div>
</section>

