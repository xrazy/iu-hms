<? Included or die(); // register

  Page::addTitle("Registration");
  
  if(User::logined()) $message = 'Log out before register again';
  else if(isset($_REQUEST['register'])){
    $email = filter($_REQUEST['register']['email']);
    $pass = filter($_REQUEST['register']['password']);
    $name = filter($_REQUEST['register']['name']);
    $phone = filter($_REQUEST['register']['phone']);
    $register = User::register($email, $pass, $name, $phone);
    $message = $register ? 'You have successfuly registred' : 'Wrong input data';
  } else $message = null;
  
?>
<section class="main">
  <div class="container content-medium bg-light">
  
    <h2><?=Page::title();?></h2>
    
    <?if(!empty($message)):?><p><?=$message;?></p><?endif;?>
    
    <? if(User::logined()){ ?>
      <p><a href="/">Go to main</a></p>
    <? } else { ?>
      <? Page::insert('form.register.php'); ?>
      <p><a href="?page=auth">Log in</a></p>
    <? } ?>
    
  </div>
</section>
