<? Included or die(); // auth

  Page::addTitle("Authentication");
  
  $message = null;
  
  if(User::logined()) $message = 'You have already logined';
  
  else if(isset($_REQUEST['auth'])){
    $email = filter($_REQUEST['auth']['email']);
    $password = filter($_REQUEST['auth']['password']);
    $auth = User::auth($email, $password);
    $message = $auth ? 'You have successfuly logged in' : 'Wrong login data';
  }
  
?>
<section class="main">
  <div class="container content-medium bg-light">
  
    <h2><?=Page::title();?></h2>
    
    <?if(!empty($message)):?><p><?=$message;?></p><?endif;?>
    
    <? if(User::logined()){ ?>
      <p><a href="/">Go to main</a></p>
    <? } else { ?>
      <? Page::insert('form.auth.php'); ?>
      <p><a href="?page=register">Register</a></p>
    <? } ?>
    
  </div>
</section>
