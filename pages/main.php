<? Included or die; // main page builder

  Page::insert('model.php');
  Page::insert('rights.php');
  
  User::logined();
  
  Page::addScript('script/jquery.js');
  Page::addScript('script/main.js');
  
  Page::addStyle('style/main.css','all');
  Page::addStyle('//fonts.googleapis.com/css?family=Fira+Sans','all',false);
  
  Page::addClass('bg-calm');
  
  Page::addHeader('meta',['name'=>'viewport','content'=>'width=device-width,initial-scale=1.0']);
  
  Page::insert('main.header.php');
  Page::insert('main.menu.php');
  $pagename = 'page.'.input('page','main').'.php';
  Page::insert($pagename);
  Page::insert('main.footer.php');
  
?>