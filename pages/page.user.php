<? Included or die(); // profile
  Page::addTitle("My Profile");
?>

<?if(User::logined()){ ?>
<section class="main">
  <div class="container content-medium bg-light">
  
    <? $user = User::current(); ?>
    
    <h2><?=$user['Name'];?></h2>
    
    <p>E-mail: <?=$user['Email'];?><br>Phone: <?=$user['Phone'];?></p>
    
    <p><a href="?page=user.edit">Edit profile</a><br><a href="?page=booking.list">My bookings</a></p>
    
  </div>
</section>
<? } ?>