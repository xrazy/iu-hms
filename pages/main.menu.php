<? Included or die(); // menu block
  
  $items = [];
  $items[] = ['title'=>'Main','href'=>'?page=main'];
  $items[] = ['title'=>'Search','href'=>'?page=search'];
  
  if(User::current()['RightsLevel'] >= LEVEL_USER){
    $items[] = ['title'=>'My Bookings','href'=>'?page=booking.list'];
  }
  
  if(User::current()['RightsLevel'] >= LEVEL_ADMIN){
    $items[] = ['title'=>'TODO list','href'=>'?page=todo'];
    $items[] = ['title'=>'Database Schema','href'=>'?page=schema'];
    $items[] = ['title'=>'UI elements test','href'=>'?page=elements'];
    $items[] = ['title'=>'SQL','href'=>'?page=sql'];
  }
  
  $items[] = ['title'=>'About','href'=>'?page=about'];
  
?>
<nav class="main bg-green">
  <div class="container">
    <ul class="menu inline menu-main">
      <? foreach($items as $item) { Page::insert('item.menu.php',$item); } ?>
    </ul>
  </div>
</nav>