

<? Included or die(); // booking info
  Page::addTitle("Booking Info");
?>
<section class="main">
  <div class="container content-medium bg-light">
  
    <h2><?=Page::title();?></h2>
    
    <? if(User::logined()){ 
		$booking = Record::getByID(filter($_REQUEST['id']));
	
	?>   
	  <div class="booking-info-item"><p>Arrival: <?=$booking['ArrivalDate']?></p>
					<p>Departure: <?=$booking['DepartureDate']?></p>
					<p>Cost: <?=$booking['Cost']?></p>
					<p>Hotel: <?=$booking['Name']?></p>
					<p>Room: <?=$booking['RoomNum']?></p>
					<p>Type: <?=$booking['Type']?></p>
					<p>Locality: <?=$booking['Locality']?></p>
					
				<? if ($booking['Disabled']==0) { ?>
					<? if (time()<(strtotime($booking['ArrivalDate']))+3600*24) {?>
					<p><a href="./?page=booking.edit&id=<?=$booking['RecordID']?>">Edit your booking</a></p>
					<p><a href="./?page=booking.cancel&id=<?=$booking['RecordID']?>">Cancel your booking</a></p>
					<? } }?>
		</div>
	  
	  
	 
      
    <? } else { ?>
      <p>You should log in first!</p>
    <? } ?>
    
    <p><a href="/">Go to main</a></p>
    
  </div>
</section>