<? Included or die(); // menu block
  
  if(User::logined()){
    $items = [ ['title'=>'Log out','href'=>'?page=logout'],
               ['title'=>'My Profile','href'=>'?page=user'], ];
  } else {
    $items = [ ['title'=>'Log in','href'=>'?page=auth'],
               ['title'=>'Register','href'=>'?page=register'], ];
  }
  
?>
<div class="menu-user">
  <ul class="menu inline">
    <? foreach($items as $item) { Page::insert('item.menu.php',$item); } ?>
  </ul>
</div>