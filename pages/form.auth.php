<? Included or die(); // auth form ?>
<form class="form" method="post" action="<?=Page::url();?>">
  <input type="hidden" name="page" value="auth">
  <div class="form-line">
    <input class="field control" type="email" name="auth[email]" value="<?=input('auth')['email'];?>" placeholder="E-mail" required>
  </div><div class="form-line">
    <input class="field control" type="password" name="auth[password]" value="" placeholder="Password" required>
  </div><div class="form-line">
    <button class="button control" type="submit">Log in</button>
  </div>
</form>
