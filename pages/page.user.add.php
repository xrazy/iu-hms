<? Included or die(); // user add
  Page::addTitle("Add Customer");
?>

<section class="main">
  <div class="container content-medium">
<?if(User::logined()){ ?>
  
    <? $user = User::current(); 
	$userinfo = DB::query ("SELECT * FROM `User` `u`, `UserType` `ut` WHERE `u`.`UserTypeID`=`ut`.`UserTypeID` AND `UserID`=".$user.";");
		$userrights = $userinfo ['RightsLevel'];
		$rights = DB::query ("SELECT * FROM `UserType` WHERE `UserTypeID`<".$userrights.";");
		
		if ($userrights>1) {
			?>
			
			<form action="<?=Page::url();?>" method="post">
			<? $newuser = input('newuser');?>
				<select name="newuser[usertype]">
				<?foreach ($rights as $right) {
					if ($right['UserTypeID']>0) {?>
					<option value=<?=$right['UserTypeID']?>><?=$right['Name']?></option>
				<?} }?>
				</select>
			<input type="email" name="newuser[email]" placeholder="Enter user's email" required>
				<input type="phone" name="newuser[phone]" placeholder="Phone number">
				<input type="password" name="newuser[pass]" placeholder="Password" required>
				<input type="text" name="newuser[name]" placeholder="User name" required>
				<button type="submit">Add</button>
			</form>
			<?
			 $result=User::add($newuser['usertype'], $newuser['email'], $newuser['pass'], $newuser['name'], $newuser['phone']);
			 if ($result>0) {
				 echo '<p>User added succesfully!</p>';				 
			 } else {
				 echo '<p>Ooops! Something went horribly wrong. Try again.</p>';
			 }
			
		} else {
			echo "<p>You don't have rights to add users!</p>";
		}
	?>
    
<?} else {?>
	 <p>You should log in first to book hotel room!</p>
<?}?>
    
	<p><a href="/">Go to main</a></p>
	
  </div>
</section>
<? } ?>