<? Included or die(); // search form ?>
<form class="form" method="post" action="?page=search">

  <input type="hidden" name="page" value="search">
  
  <div class="form-line">
  
    <div class="ngrid cont">
    
      <?
        $arrival = filter($_COOKIE['date_arrival'])?:date('Y-m-d', strtotime('today +1 day'));
        $departure = filter($_COOKIE['date_departure'])?:date('Y-m-d', strtotime('today +2 day'));
      ?>
    
      <div class="part p1of3">
        <div class="label">Locality</div>
        <input id="search_locality" class="control field" type="text" name="search[city]" value="<?=$item['city'];?>" placeholder="City or area name">
      </div>
      
      <div class="part p1of3">
        <div class="label">Arrival</div>
        <input class="control field" type="text" name="search[arrival]" value="<?=$item['arrival']?:$arrival;?>" placeholder="Arrival date">
      </div>
      
      <div class="part p1of3">
        <div class="label">Departure</div>
        <input class="control field" type="text" name="search[departure]" value="<?=$item['departure']?:$departure;?>" placeholder="Departure date">
      </div>
      
    </div>
    
  </div>
  
  <div id="advSearch">
    
    <div class="form-line">
    
      <div class="ngrid cont">
      
        <div class="part p1of3">
          <div class="label">Room Type</div>
          <select class="control field" name="search[roomtype]">
            <option value="">Any</option>
          </select>
        </div>
        
        <div class="part p1of3">
          <div class="label">Price</div>
          <input class="control field" type="text" name="search[maxprice]" value="<?=$item['maxprice'];?>" placeholder="Maximum price">
        </div>
        
        <div class="part p1of3">
          <div class="label">Hotel Rating</div>
          <input class="control field" type="text" name="search[rating]" value="<?=$item['rating'];?>" placeholder="From 1 to 5">
        </div>
        
      </div>
      
    </div>
    
    <div class="form-line">
      Features *
    </div>
    
    <div class="form-line">
      Order by *
    </div>
    
  </div>
  
  <div class="form-line">
    <button class="control button" type="submit">Search</button>
    <a id="advSearchOpen" class="button" href="#">Advanced search</a>
  </div>
  
</form>
<?
  $script = '$(document).ready(function() {
    var advSearch = $("#advSearch"), advSearchOpen = $("#advSearchOpen");
    advSearchOpen.on("click", function(){ advSearch.slideToggle(); });
  });';
  Page::addScriptRaw($script);
?>