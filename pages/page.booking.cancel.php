<? Included or die(); // booking edit
  Page::addTitle("Cancel your booking");
?>
<section class="main">
  <div class="container content-medium">
  
    <h2><?=Page::title();?></h2>
    <? if(User::logined()){ ?>
		<? $booking = Record::getByID($_REQUEST['id']);
			
			
			if(!empty($booking)) {
				$cancel = input ('cancel');
				if ($cancel['confirmed']=='true') {
					$result = Record::disable($cancel['id']);
					if ($result>0)
					{
							echo "<p>Successfully canceled. Don't worry if you see it in your profile, bacause we keep all the information. In fact, it IS canceled </p>";
					}
					
				}	
			}
		?>
		
		<div class="booking-info-item"><p>Arrival: <?=$booking['ArrivalDate']?></p>
					<p>Departure: <?=$booking['DepartureDate']?></p>
					<p>Cost: <?=$booking['Cost']?></p>
					<p>Hotel: <?=$booking['Name']?></p>
					<p>Room: <?=$booking['RoomNum']?></p>
					<p>Type: <?=$booking['Type']?></p>
					<p>Locality: <?=$booking['Locality']?></p>
			<form action="<?=Page::url();?>" method="post"> 
				<input type="hidden" name="cancel[confirmed]" value="true">
				<input type="hidden" name="cancel[id]" value="<?=$_REQUEST['id']?>">
				<input type="submit" value="I am sure I want to cancel this booking">
			</form>
		</div>
	
      
    <? } else { ?>
      <p>You should log in first!</p>
    <? } ?>
		
		<p><a href="/">Go to main</a></p>
    
  </div>
</section>