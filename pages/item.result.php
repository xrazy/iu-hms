<? Included or die(); // menu item ?>
<div class="search-item">
  <span class="search-item-title"><?=$item['Name'];?></span>
  <span class="search-item-stars">[<? while($item['StarRank']-->0)echo"★";?>]</span>
  <span class="search-item-room">Room #<?=$item['RoomNum'];?></span>
  <span class="search-item-price">Price: <?=$item['Price'];?></span>
  <a class="search-item-book" href="?page=book&roomid=<?=$item['RoomID'];?>">Book this room</a>
</div>
