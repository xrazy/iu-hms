<? Included or die(); // sql query

  if(User::logined()) {
    
    if(User::current()['RightsLevel'] >= LEVEL_ADMIN){
    
    Page::addTitle("SQL query");
    
?>
<section class="main">
  <div class="container content-medium bg-light">
  
    <h2><?=Page::title();?></h2>
    
    <? $sql = input('sql'); ?>
    
    <form class="form" method="post" action="<?=Page::url();?>">
      <div class="form-line">
        <textarea class="field control" name="sql[query]"><?=$sql['query'];?></textarea>
      </div><div class="form-line">
        <button class="button control" type="submit" name="sql[submit]" value="query">Query</button>
        <button class="button control" type="submit" name="sql[submit]" value="exec">Exec</button>
      </div>
    </form>
    
    <?
      if($sql['submit']=='query'){ echo '<pre>'.var_export(DB::query($sql['query']),true).'</pre>'; }
      else if($sql['submit']=='exec'){ echo '<p>Tuples affected: '.DB::exec($sql['query']).'</p>'; }
    ?>
    
  </div>
</section>
<? } } ?>