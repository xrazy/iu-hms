<? Included or die(); // search page
  Page::addTitle("Search");
?>
<section class="main">
  <div class="container content-medium bg-light">
  
    <h2><?=Page::title();?></h2>
    
    <div class="search-top">
      <?
        $cities = Hotel::getLocations();
        foreach($cities as $city){
          echo '<a class="search-locality" title="'.$city['Locality'].'" href="#">'.$city['Locality'].'</a>'."\n";
        }
        $script = '$(document).ready(function() {
          var locInput = $("#search_locality");
          $("a.search-locality").on("click", function(el){
            el.preventDefault();
            var locality = el.target.title;
            locInput.val(locality);
          });;
        });';
        Page::addScriptRaw($script);
      ?>
    </div>
    
    <?
    
      $query = input('search');
      
      Page::insert('form.search.php',$query);
      
      // main params
      $city = filter($query['city']);
      
      $arrival = filter($query['arrival']);
      $departure = filter($query['departure']);
      
      setcookie('date_arrival', $arrival);
      setcookie('date_departure', $departure);
      
      // other params
      
      $results = Room::getByLocation($city, $arrival, $departure);
      
      if(!empty($city) && empty($results)) {
        echo '<p>Nothing found :(</p>';
      } else {
        if(!empty($results)) echo '<h3>Results</h3>';
        foreach($results as $item){
          Page::insert('item.result.php',$item);
        }
      }
      
    ?>
    
  </div>
</section>
