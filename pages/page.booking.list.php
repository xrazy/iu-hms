<? Included or die(); // booking list
  Page::addTitle("Booking List");
?>
<section class="main">
  <div class="container content-medium bg-light">
  
    <h2><?=Page::title();?></h2>
    
    <? if(User::logined()){ ?>
    
      <? 
		$bookings = Record::getByUser(User::current()['UserID']);
		if (empty($bookings)) { ?>
			<p>You don't have any bookings now!</p>
		<? } else { ?>
			<? foreach ($bookings as $booking) { ?>
				<div class="booking-info-item">
          <p>Arrival: <?=$booking['ArrivalDate']?><br>
            Departure: <?=$booking['DepartureDate']?><br>
            Cost: <?=$booking['Cost']?><br>
            Locality: <?=$booking['Locality']?><br>
			<?if ($booking['Disabled']) {?>
			Canceled or finished <br>
			<?} else {?>
			Active<br>
			<?}?>
            <a href="./?page=booking.info&id=<?=$booking['RecordID']?>">More info</a>
          </p>
				</div>
			
		<? } } ?>
	  
      
    <? } else { ?>
      <p>You should log in first!</p>
    <? } ?>
    
    <p><a href="/">Go to main</a></p>
    
  </div>
</section>