<? Included or die(); // logout
  Page::addTitle("Log out");
  if(User::logined()) User::logout();
?>
<section class="main">
  <div class="container content-medium">
  
    <h2><?=Page::title();?></h2>
    
    <? if(User::logined()){ ?>
      <p>Error: can't log out!</p>
    <? } else { ?>
      <p>You have loged out successfully!</p>
    <? } ?>
    
    <p><a href="/">Go to main</a></p>
    
  </div>
</section>