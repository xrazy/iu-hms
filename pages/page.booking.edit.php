<? Included or die(); // edit booking
  Page::addTitle("Edit Booking");
?>
<section class="main">
  <div class="container content-medium">
  
    <h2><?=Page::title();?></h2>
    
    <? if(User::logined()){ 
		$user = User::current(); 
		$userinfo = DB::query ("SELECT * FROM `User` `u`, `UserType` `ut` WHERE `u`.`UserTypeID`=`ut`.`UserTypeID` AND `UserID`=".$user.";");
		$userrights = $userinfo ['RightsLevel'];
		$rights = DB::query ("SELECT * FROM `UserType` WHERE `UserTypeID`<".$userrights.";");
		$booking = Record::getByID($_REQUEST['id'])
	?>
		<div> 
			<div>
				<form action="<?=Page::url();?>" method="post">
				<div class="label">Change Arrival Date</div>
				<input class="control field" type="text" name="arrivaldate" value="<?=$booking['ArrivalDate']?>">
				<button type="submit">Change</button>
				</form>
			</div>
			<div>
				<form action="<?=Page::url();?>" method="post">
				<div class="label">Change Departure Date</div>
				<input class="control field" type="text" name="departuredate" value="<?=$booking['DepartureDate']?>">
				<button type="submit">Change</button>
				</form>
			</div>
			<? if ($userrights>1) { ?>
			<div>
				<form action="<?=Page::url();?>" method="post">
				<div class="label">Change Status</div>
				<input class="control other" type="checkbox" name="disabled" value="<?=$booking['Disabled']?'on':'off'?>"><label>Disabled</label>
				<button type="submit">Change</button>
				</form>
			</div>
			<div>
				<form action="<?=Page::url();?>" method="post">
				<div class="label">Change Paid Amount</div>
				<input class="control field" type="text" name="paidamount" value="<?=$booking['PaidAmount']?>">
				<button type="submit">Change</button>
				</form>
			</div>
			<? } ?>
		</div>      
    <?
		$arrivaldate=$_REQUEST['arrivaldate'];
		$departuredate=$_REQUEST['departuredate'];
		$disabled=$_REQUEST['disabled']=='on'?true:false;
		$paidamount=$_REQUEST['paidamount'];
		$result=0;
		if (!empty($arrivaldate)) {
			$result+= Record::changeArrival($booking['RecordID'], $arrivaldate);
		}
		if (!empty ($departuredate)) {
			$result+= Record::changeDeparture($booking['RecordID'], $departuredate);
		}
		if (!empty($disabled)) {
			$result+= Record::disable($booking['RecordID']);
		}
		if (!isset($paidamount)) {
			$result+= Record::changePayments($booking['RecordID'], $paidamount);
		}
		Record::assignReceptionist($booking['RecordID'], $user);
		if ($result){
			echo "<p>Successful changes! You can continue messing with this record.</p>";			
		} else {
			echo "<p>Something went wrong. Try again or notify administrator of the site. Or just go do something else because you cannot do it anymore</p>";
		}
	} else { ?>
      <p>You should log in first!</p>
    <? } ?>
    
    <p><a href="/">Go to main</a></p>
    
  </div>
</section>