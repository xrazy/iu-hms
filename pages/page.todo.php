<? Included or die(); // todo
  Page::addTitle("TODO list");
?>
<section class="main">
  <div class="container content-medium bg-light">
  
    <h2><?=Page::title();?></h2>
    
    <p><a href="//docs.google.com/document/d/164ZIOx9NfHFc00kabs7VNmBTAF-rVO0XKJ4An8h4dh0/edit">Terms of Reference</a></p>
    
    <ul>
      <li><del><a href="//docs.google.com/spreadsheets/d/150gXcxC3utT3kFADthXyIyqKx7HthDlzZFjv3akm_1k/edit">User Stories</a></del></li>
      <li>ER Diagram (main entities & ralationships)</li>
      <li><del><a href="/pages/files/DataBaseRelations.txt">Schema</a> (all tuples)</del></li>
      <li><del>Normalization</del></li>
      <li><del>Database (SQL queries to create tables)</del></li>
      <li>Views + <del>Constraints</del> + Triggers</li>
      <li>WEB application:
        <ul>
          <li><del>Page output</del></li>
          <li>UI Design</li>
          <li><del>User Auth / Reg</del></li>
          <li>Layout (forms, lists, etc)</li>
          <li><del>Model classes</del></li>
          <li><a href="/pages/files/Pages.txt">Pages</a> *</li>
        </ul>
      </li>
      <li>Setup <a href="setup.php">Script</a></li>
      <li>Generated Data *</li>
      <li><a href="readme.txt">readme.txt</a> *</li>
      <li>Report *</li>
      <li><a href="//docs.google.com/presentation/d/12ZgBBS-db67WW3NbzcJXcV5jizu2uduvPsrpnHxYzKA/edit">Presentation</a></li>
    </ul>
    
  </div>
</section>
