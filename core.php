<? // core classes & functions
  
  define('Included', true);
  
  // include config
  include_once 'config.php';
  
  class Page {
    
    /* Page Headers */
    
    static private $headers = [];
    static private $headers_raw = "";
    
    static function headers(){
      return implode("\n",Page::$headers).$headers_raw."\n";
    }
    
    static function addHeader($tag,$params=[],$content=null){
      $header = '<'.$tag;
      foreach($params as $parname => $parvalue){
        $header.= ' '.$parname.'="'.$parvalue.'"';
      } $header.= ($content==null) ? '/>' : '>'.$content.'</'.$tag.'>';
      Page::$headers[] = $header;
    }
    
    static function addHeaderRaw($html){
      $headers_raw.= "\n".$html;
    }
    
    /* Page Styles */
    
    static function addStyle($href, $media=null, $internal=true){
      if($internal) $href= self::root().$href;
      $params = ['rel' => 'stylesheet', 'href' => $href];
      if($media!=null) $params['media'] = $media;
      Page::addHeader('link', $params);
    }
    
    static function addStyleRaw($css){
      $headers_raw.= '<style>'.$css.'</style>'."\n";
    }
    
    /* Page Scripts */
    
    static private $scripts = [];
    static private $scripts_raw = "";
    
    static function scripts(){
      $result = "";
      foreach(self::$scripts as $src){
        $result.= '<script src="'.$src.'"></script>'."\n";
      }
      return $result.self::$scripts_raw;
    }
    
    static function addScript($src){ self::$scripts[] = self::root().$src; }
    
    static function addScriptRaw($code){
      self::$scripts_raw.= '<script>'.$code.'</script>'."\n";
    }
    
    /* Page Title */
    
    static private $title = [];
    
    static function title($reverse=true,$glue=null){
      $parts = $reverse ? array_reverse(self::$title) : self::$title;
      if(empty($glue)) return $parts[0];
      else return implode($glue,$parts);
    }
    
    static function addTitle($title){
      self::$title[] = $title;
    }
    
    /* Page Root */
    
    static private $pageroot = "";
    
    static function root(){ return self::$pageroot; }
    
    static function setRoot($path){ self::$pageroot = $path; }
    
    /* body params */
    
    static private $body_class = [];
    
    static function addClass($classname){ self::$body_class[] = $classname; }
    
    static function bodyClass(){
      return empty(self::$body_class) ? '' : ' class="'.implode(' ', self::$body_class).'"';
    }
    
    /* Page */
    
    static function insert($filename,$item=null){
      $filename = self::$pageroot.$filename;
      if(file_exists($filename)) include $filename;
      else { echo 'File "'.$filename.'" not found.'; }
    }
    
    static function render($content){
      $output = '<!DOCTYPE html>'."\n";
      $output.= '<html>'."\n".'<head>'."\n";
      $output.= '<meta charset="'.PAGE_CHARSET.'">'."\n";
      $output.= '<title>'.self::title(true, PAGE_TITLE_GLUE).'</title>'."\n";
      $output.= self::headers();
      $output.= '</head>'."\n".'<body'.self::bodyClass().'>'."\n";
      $output.= $content."\n";
      $output.= self::scripts();
      $output.= '</body>'."\n".'</html>'."\n";
      print $output;
    }
    
    static function base(){
      return '/'.SITE_ROOT;
    }
    
    static function url(){
      return $_SERVER['REQUEST_URI'];
    }
    
  }
  
  class DB {
    
    static private $DB;
    
    static function init($dbhost, $dbname, $user, $pass){
      try {
        self::$DB = new PDO('mysql:host='.$dbhost.';dbname='.$dbname, $user, $pass);
      } catch (Exception $ex) {
        die($ex->getMessage());
      }
    }
    
    static function query($query,$multiple=true){
      
      $result = self::$DB->query($query);
      
      if($result==null){
        echo "\nError in query: ".$query; // not secure!
      } else if($result->rowCount()>0){
        if($multiple){ $result = $result->fetchAll(); }
        else { $result = $result->fetch(); }
      } else { $result = []; }
      
      return $result;
      
    }
    
    static function exec($query,$lastid=false){
      $result = self::$DB->exec($query);
      return $lastid ? self::$DB->lastInsertId() : $result;
    }
    
    static function lastID(){ return self::$DB->lastInsertId(); }
    
    static function pdo(){ return self::$DB; }
    
  }
  
  /* old shift */
  
  // redirects user to address
  function jump($where){
    header('Location: '.$where);
  }
  
  // returns default if no input
  function input($name, $default=false){
    $input = request($name);
    return empty($input) ? $default : $input;
  }
  
  // returns user input 
  function request($name){
    return isset($_REQUEST[$name]) ? $_REQUEST[$name] : null;
  }
  
  // filter data before adding to database
  function filter($string,$type=FILTER_SANITIZE_MAGIC_QUOTES){
    return filter_var(trim($string),$type);
  }
  
  // send utf8 email
  function utf8mail($to_addr,$subject="",$mail_body,$from_addr=null,$from_name=null,$reply_addr=null) {
    $headers="MIME-Version: 1.0\r\nContent-Type: text/html; charset=utf-8\r\n";
    if(!empty($subject)) $subject="=?utf-8?b?".base64_encode($subject)."?=";
    if(!empty($from_name)) $from_name = '=?utf-8?b?'.base64_encode($from_name).'?= ';
    if(!empty($from_addr)) $headers.= 'From: '.$from_name.'<'.$from_addr.'>'."\r\n";
    if(!empty($reply_addr)) $headers.= 'Reply-To: '.$reply_addr."\r\n";
    $headers.= 'X-Mailer: PHP/'.phpversion()."\r\n";
    return mail($to_addr,$subject,$mail_body,$headers);
  }
  
// lastphpeng 0.1 by le5h ?>