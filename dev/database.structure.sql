SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;


CREATE TABLE IF NOT EXISTS `Address` (
  `AddressID` int(11) NOT NULL AUTO_INCREMENT,
  `Country` varchar(255) NOT NULL,
  `Locality` varchar(255) NOT NULL,
  `AddressDetails` varchar(255) DEFAULT NULL,
  `PostalCode` varchar(10) NOT NULL,
  `Latitude` decimal(7,5) NOT NULL,
  `Longitude` decimal(8,5) NOT NULL,
  PRIMARY KEY (`AddressID`),
  UNIQUE KEY `AddressID` (`AddressID`,`AddressDetails`,`PostalCode`,`Latitude`,`Longitude`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `Employee` (
  `UserID` int(10) unsigned NOT NULL,
  `HotelID` int(10) unsigned NOT NULL,
  `Salary` int(10) unsigned NOT NULL DEFAULT '0',
  `Removed` tinyint(1) DEFAULT '0',
  UNIQUE KEY `UserID` (`UserID`),
  KEY `HotelID` (`HotelID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `Feature` (
  `FeatureID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Name` varchar(255) DEFAULT NULL,
  `Description` text,
  PRIMARY KEY (`FeatureID`),
  UNIQUE KEY `Name` (`Name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `FeatureValue` (
  `FeatureValueID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `FeatureID` int(10) unsigned NOT NULL,
  `Value` text NOT NULL,
  PRIMARY KEY (`FeatureValueID`),
  KEY `FeatureID` (`FeatureID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `Hotel` (
  `HotelID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Disabled` tinyint(1) DEFAULT '0',
  `Name` varchar(255) NOT NULL,
  `AddressID` int(11) unsigned NOT NULL,
  `StarRank` int(11) unsigned NOT NULL,
  `Phone` varchar(25) DEFAULT NULL,
  `OwnerID` int(11) unsigned NOT NULL,
  PRIMARY KEY (`HotelID`),
  KEY `AddressID` (`AddressID`),
  KEY `OwnerID` (`OwnerID`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `Login` (
  `TimeStamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `UserID` int(11) NOT NULL,
  `LoginHash` varchar(64) NOT NULL,
  PRIMARY KEY (`LoginHash`),
  KEY `UserID` (`UserID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `Record` (
  `RecordID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `TimeStamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `UserID` int(10) unsigned NOT NULL,
  `ReceptionistID` int(10) unsigned NOT NULL,
  `RoomID` int(10) unsigned NOT NULL,
  `ArrivalDate` date NOT NULL,
  `DepartureDate` date NOT NULL,
  `Cost` int(10) unsigned NOT NULL,
  `Disabled` tinyint(1) NOT NULL DEFAULT '0',
  `PaidAmount` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`RecordID`),
  KEY `UserID` (`UserID`),
  KEY `RoomID` (`RoomID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `RecordComment` (
  `CommentID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `TimeStamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `RecordID` int(10) unsigned NOT NULL,
  `UserID` int(10) unsigned NOT NULL,
  `Comment` text NOT NULL,
  PRIMARY KEY (`CommentID`),
  KEY `UserID` (`UserID`),
  KEY `RecordID` (`RecordID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `Room` (
  `RoomID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `HotelID` int(11) unsigned NOT NULL,
  `RoomNum` int(10) unsigned NOT NULL,
  `Price` int(10) unsigned NOT NULL,
  `Disabled` tinyint(1) DEFAULT '0',
  `RoomTypeID` int(11) NOT NULL,
  PRIMARY KEY (`RoomID`),
  KEY `HotelID` (`HotelID`),
  KEY `RoomTypeID` (`RoomTypeID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `RoomType` (
  `RoomTypeID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `TypeName` varchar(255) NOT NULL,
  PRIMARY KEY (`RoomTypeID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `TypeFeature` (
  `TypeFeatureID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `HotelID` int(10) unsigned NOT NULL,
  `RoomTypeID` int(10) unsigned NOT NULL,
  `FeatureValueID` int(10) unsigned NOT NULL,
  PRIMARY KEY (`TypeFeatureID`),
  KEY `HotelID` (`HotelID`),
  KEY `RoomTypeID` (`RoomTypeID`),
  KEY `FeatureValueID` (`FeatureValueID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `User` (
  `UserID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `UserTypeID` int(11) unsigned NOT NULL,
  `Name` varchar(255) NOT NULL,
  `Email` varchar(255) DEFAULT NULL,
  `Phone` varchar(255) DEFAULT NULL,
  `Password` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`UserID`),
  UNIQUE KEY `Email` (`Email`),
  UNIQUE KEY `Phone` (`Phone`),
  KEY `UserTypeID` (`UserTypeID`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `UserType` (
  `UserTypeID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Name` varchar(60) NOT NULL,
  `RightsLevel` int(11) unsigned NOT NULL,
  PRIMARY KEY (`UserTypeID`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
