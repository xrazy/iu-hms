INSERT INTO `RoomType` (`RoomTypeID`, `TypeName`) VALUES
(1, 'Penthouse'),
(2, 'Komorka'),
(3, 'Standard'),
(4, 'Luxe'),
(5, 'Junior'),
(6, 'Bridal'),
(7, 'Hostel');
