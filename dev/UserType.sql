INSERT INTO `UserType` (`UserTypeID`, `Name`, `RightsLevel`) VALUES
(4, 'Owner', 4),
(3, 'Manager', 3),
(2, 'Receptionist', 2),
(1, 'Customer', 1),
(0, 'Guest', 0),
(5, 'Admin', 10);
