<?php
class Record {
	
	static function getByID ($id){
		return DB::query("SELECT `r`.*, `rm`.`RoomNum` AS `RoomNum`, `h`.`Name` AS `Name`, `a`.`Locality` as `Locality`, `rt`.`TypeName` AS `Type`  FROM `Record` `r`, `Room` `rm`, `Hotel` `h`, `Address` `a`, `RoomType` `rt` WHERE `r`.`RecordID`=".$id." AND `r`.`RoomID`=`rm`.`RoomID` AND `rm`.`HotelID`=`h`.`HotelID` AND `h`.`AddressID`=`a`.`AddressID` AND `rm`.`RoomTypeID`=`rt`.`RoomTypeID`;", false);
	}
	
	static function getByUser ($id){
		return DB::query("SELECT `r`.*, `rm`.`RoomNum` AS `RoomNum`, `h`.`Name` AS `Name`, `a`.`Locality` as `Locality`, `rt`.`TypeName` AS `Type`  FROM `Record` `r`, `Room` `rm`, `Hotel` `h`, `Address` `a`, `RoomType` `rt` WHERE `r`.`UserID`=".$id." AND `r`.`RoomID`=`rm`.`RoomID` AND `rm`.`HotelID`=`h`.`HotelID` AND `h`.`AddressID`=`a`.`AddressID` AND `rm`.`RoomTypeID`=`rt`.`RoomTypeID`;");
	}
	
	static function getByHotel ($hotel){
		$s = "SELECT `rc`.* FROM `Record` `rc`, `Room` `rm` WHERE ";
		$s.= "`rc`.`RoomID`=`rm`.`RoomID` AND `rm`.`HotelID`=".$hotel.";";
		return DB::query ($s);
	}
	
	static function getByReceptionist ($id){
		return DB::query("SELECT * FROM `Record` WHERE `ReceptionistID`=".$id.";");
	}
	
	static function create ($user, $receptionist, $room, $arrival, $departure){
		$check = User::getByID($user);
		if (empty($check)) { return -1; }
		if (strtotime($departure) < strtotime($arrival)) { return -1; }
		$check = "SELECT * FROM `Record` WHERE `RoomID` = ".$room." AND NOT ";
		$check.= "(`ArrivalDate` > '".$departure."' OR `DepartureDate` < '".$arrival."');";
		$check = DB::query($check);
		if (!empty($check)) { return -1; }
		$roomDetails = Room::getByID($room);
		$cost = (int) (((strtotime($departure) - strtotime($arrival)) / 3600 / 24 ) * $roomDetails['Price']);
		$s = "INSERT INTO `Record` VALUES (NULL, NULL, ".$user.", ".$receptionist.", ";
		$s.= $room.", '".$arrival."', '".$departure."', ".$cost.", '0', '0');";
		if(DB::exec($s)==0) { return $s; }
    else { return DB::lastID(); }
	}
	
	# all changes can only be performed to enabled records
	
	static function changeArrival ($id, $newArrival){
		if (strtotime($newArrival) <= (time()+24*3600)) {
			return 0;
		}
		$temp = self::getByID($id);
		if (empty($temp)) {
			return 0;
		} 
		if ($temp['Disabled']) {
			return 0;
		}
		if (strtotime($temp['ArrivalDate']) <= (time()+24*3600)) {
			return 0;
		}
		if (strtotime($temp['DepartureDate'])<=strtotime($newArrival)) {
			return 0;
		}
		
		$check = "SELECT * FROM `Record` WHERE `RoomID`=".$room." AND `RecordID`<>".$id." AND NOT ";
		$check.= "(`ArrivalDate`>'".$temp['DepartureDate']."' OR `DepartureDate`<'".$newArrival."');";
		$check = DB::query($check);
		if (!empty($check)) {
			return 0;
		}
		
		$newCost = $temp['Cost']/(strtotime($temp['DepartureDate'])-strtotime($temp['ArrivalDate']));
		$newCost*= (strtotime($temp['DepartureDate'])-strtotime($newArrival));
		$s = "UPDATE `Record` SET `ArrivalDate`='".$newArrival."', `Cost`=".$newCost." WHERE `RecordID`=".$id.";";
		return DB::exec($s);
	}
	
	static function changeDeparture ($id, $newDeparture){
		if (strtotime($newDeparture)<time()) {
			return 0;
		}
		$temp = self::getByID($id);
		if (empty($temp)) {
			return 0;
		}
		if ($temp['Disabled']) {
			return 0;
		}
		if (strtotime($newDeparture) <= strtotime($temp['ArrivalDate'])) {
			return 0;
		}
		
		$check = "SELECT * FROM `Record` WHERE `RoomID`=".$room." AND `RecordID`<>".$id." AND NOT ";
		$check.= "(`ArrivalDate`>'".$newDeparture."' OR `DepartureDate`<'".$temp['ArrivalDate']."');";
		$result = DB::query($check);
		if (!empty($result)) {
			return 0;
		}
		
		$newCost=$temp['Cost'];
		if (strcmp($newDeparture,$temp['DepartureDate'])<0) {
			$newCost*= (strtotime($newDeparture)-strtotime($temp['ArrivalDate']));
			$newCost/= (strtotime($temp['DepartureDate'])-strtotime($temp['ArrivalDate']));
		} elseif (strcmp($newDeparture,$temp['DepartureDate'])>0) {
			$room = Room::getByID($temp['RoomID']);
			$newCost+= $room['Price']*(strtotime($newDeparture)-strtotime($temp['DepartureDate']))/(3600*24);
		}
		$s = "UPDATE `Record` SET `DepartureDate`='".$newDeparture."', `Cost`=".$newCost." WHERE `RecordID`=".$id.";";
		return DB::exec($s);
	}
	
	static function disable ($id){
		return DB::exec("UPDATE `Record` SET `Disabled`='1' WHERE `RecordID`=".$id.";");
	}
	
	static function disableMany (){
		$records = "SELECT * FROM `Record` WHERE `DepartureDate`<'".date('Y-m-d')."' AND `Cost`=`PaidAmount`;";
		$records = DB::query($records);
		foreach ($records as $rec) {
			disable($rec['RecordID']);
		}
	}
	
	static function assignReceptionist ($record, $receptionist ){
		$check = Employee::getByID($receptionist);
		if (empty($check)) {
			return 0;
		}
		$temp = self::getByID($record);
		if ($temp['Disabled']) {
			return 0;
		}
		if ($temp['ReceptionistID']!==NULL) {
			return 0;
		}
		$s = "UPDATE `Record` SET `ReceptionistID`=".$receptionist." WHERE `RecordID`=".$record.";";
		return DB::exec($s);
	}
	
	static function changePayments ($id, $payment){
		if ($payment<0) {
			return 0;
		}
		$rec = self::getByID();
		if ($rec['Disabled']) {
			return 0;
		}
		$result = "UPDATE `Record` SET `PaidAmount`=".$payment." WHERE `RecordID`=".$id.";";
		$result = DB::exec($result);
		self::disableMany();
		return $result;
	}
		
}
