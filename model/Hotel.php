<?php

class Hotel {
	
	static function getByID ($id){
		$s = "SELECT * FROM `Hotel` WHERE `HotelID`='".$id."';";
		$result = DB::query($s, false);
		return $result;
	}
	
	static function getByCity ($city){
		$s = "SELECT `h`.* FROM `Hotel` `h`, `Address` `a` WHERE `h`.`AddressID`=`a`.`AddressID` AND `Locality`='".$city."';";
		$result = DB::query($s);
		return $result;
	}
	
	static function getByCityAndStars ($city, $stars) {
		$s = "SELECT `h`.* FROM `Hotel` `h`, `Address` `a` WHERE `h`.`AddressID`=`a`.`AddressID` AND `Locality`='".$city."' AND ";
		$s.= "`StarRank`='".$stars."';";
		$result = DB::query($s);
		return $result;
	}
	
	static function getByOwner ($owner){
		$s = "SELECT * FROM `Hotel` WHERE `OwnerID`='".$owner."';";
		$result = DB::query($s);
		return $result;
	}
	
	static function getOwner ($hotel){
		$s = "SELECT `OwnerID` FROM `Hotel` WHERE `HotelID`='".$hotel."';";
		$result = DB::query($s, false);
		return $result;
	}
	
	static function add($name, $address, $stars, $owner){
		if (($name===NULL) || ($name=="")){ // no null names, no empty names
			return -1;
		}
		$check = User::getByID($owner);
		if (empty($check)) {
			return -1;
		}
		$s = "INSERT INTO `Address` VALUES ('NULL', '".$address['Country']."', '";
		$s.= $address['Locality']."', '".$address['AddressDetails']."', '".$address['PostalCode'];
		$s.= "', '".$address['Latitude']."', '".$address['Longitude']."');";
		if (DB::exec($s)==0){
			return -1;
		} else {
			$address = DB::lastID();
		}
		$s = "INSERT INTO `Hotel` VALUES ('NULL', '0', '".$name."', '".$address."', '".$stars."', '".$owner."');";
		$result = DB::exec($s);
		if ($result == 0) {
			return -1;
		} else {
			return DB::lastID();
		}
	}
	
	static function setStatus ($id, $status){
		$s = "UPDATE `Hotel` SET `Disabled`='".$status."' WHERE `HotelID`='".$id."';";
		$result = DB::exec($s);
		return $result;
	}
	
	static function renaming ($id, $newName){
		if (($newName===NULL) || ($newName=="")){ // no null names, no empty names
			return 0;
		}
		$s = "UPDATE `Hotel` SET `Name`='".$newName."' WHERE `HotelID`='".$id."';";
		$result = DB::exec($s);
		return $result;
	}
	
	static function reStar ($id, $stars){
		$s = "UPDATE `Hotel` SET `StarRank`='".$stars."' WHERE `HotelID`='".$id."';";
		$result = DB::exec($s);
		return $result;
	}
	
	static function changeOwner ($id, $newOwner){
		$check = User::getByID($newOwner); 
		if (empty($check)) {
			return 0;
		}
		$s = "UPDATE `Hotel` SET `OwnerID`='".$newOwner."' WHERE `HotelID`='".$id."';";
		$result = DB::exec($s);
		return $result;
	}
	
  static function getAddress($id){
    $query = 'SELECT * FROM `Address` WHERE `AddressID` = '.$id.';';
    return DB::query($query,false);
  }
  
  static function getLocations($max = 10){
    $query = 'SELECT `Locality`, COUNT(*) FROM `Address` '.
             'GROUP BY 1 ORDER BY 2 DESC LIMIT '.$max.';';
    return DB::query($query);
  }
  
}
