<?php
class User {
  
	static function getByID($id){
    $query = 'SELECT * FROM `User` '.
             'LEFT JOIN `UserType` ON (`UserType`.`UserTypeID` = `User`.`UserTypeID`) '.
             'WHERE `UserID` = '.$id.';';
		return DB::query($query,false);
	}
	
	static function getByEmail($email){
    $query = 'SELECT * FROM `User` '.
             'LEFT JOIN `UserType` ON (`UserType`.`UserTypeID` = `User`.`UserTypeID`) '.
             'WHERE LOWER(`Email`) = "'.$email.'";';
		return DB::query($query, false);
	}
	
	static function getByPhone($phone){
    $query = 'SELECT * FROM `User` '.
             'LEFT JOIN `UserType` ON (`UserType`.`UserTypeID` = `User`.`UserTypeID`) '.
             'WHERE `Phone` = "'.$phone.'";';
		return DB::query($query);
	}
	
	# returns all users with a given usertype # maybe better call it getUsersOfType()?
	static function getUserType ($type){
		$query = 'SELECT * FROM `User` `u`, `UserType` `ut` WHERE `u`.`UserTypeID`=`ut`.`UserTypeID` AND `UserTypeID`="'.$type.'";';
		return DB::query($query);
	}
	
	static function changeEmail ($id, $newEmail){
		$regexp = "/^\w[-\w.+]*@([A-z0-9][-A-z0-9]+\.)+[A-z]{2,4}$/"; // regexp taken from stackoverflow
		if (preg_match($regexp, $newEmail)!=1){ return 0;	}
    // there can't be two users with same emails
		$check = self::getByEmail($newEmail);
		if (!empty($check)){ return 0; }
		$s = "UPDATE `User` SET `Email`='".$newEmail."' WHERE `UserID`='".$id."';";
		$result = DB::exec($s);
		return $result;
	}
	
	static function changePhone ($id, $newPhone){
    // self-made regexp, allows for +, () for prefix code, up to 6 '-' between digits
		$regexp = "/^\+?\d{1}\(?\d{3}\)?(\d{1}-?){6}\d{1}$/"; 	
		// requires exactly 11 digits
		if (($newPhone!==NULL) && (preg_match($regexp, $newPhone)!=1)){  //new phone number must be correct or null
			return 0;
		}
		$s = "UPDATE `User` SET `Phone`='".$newPhone."' WHERE `UserID`='".$id."';";
		$result = DB::exec($s);
		return $result;
	}
	
  // change pass
	static function changePassword ($id, $oldpass, $newpass){
    $user = self::getByID($id);
		if(self::passhash($oldpass)==$user['Password']&&self::validatePassword($newpass)){
      return self::setPassword($id, self::passhash($newpass));
    } else return false;
	}
  
  // sets password
  static function setPassword($id, $pass=null){
    if($pass==null) $pass = self::generatePassword();
    $query = 'UPDATE `User` SET `Password` = "'.$pass.'" WHERE `UserID` = '.$id.';';
    return DB::exec($query);
  }
  
  // generate a password
  static function generatePassword($len=8){
    $pass = "";
    for ($i=0; $i<$len; $i++) { $pass.= chr(rand(32,126)); }
    return $pass;
  }
  
  static function validatePassword($pass){
    $def_passlen = 7; // bye qwerty
    return strlen($pass)<$def_passlen;
  }
  
  // register new user
  static function register($email, $pass, $name=null, $phone=null){
    
    $def_utype = 1; // customer
    
    // check only for length (because we are not monsters)
    if(self::validatePassword($pass)) return false;
      
    // prepare data
    if(empty($name)) $name = "Unknown";
    
    return self::add($def_utype, $email, $pass, $name, $phone);
    
  }
  
  // add new user
  static function add($usertype, $email, $pass, $name, $phone){
    
    $regex_email = '/.+@.+/';
    
    if(!is_numeric($usertype) && $usertype<0){ return false; }
    if(empty($email) || empty($pass) || empty($name)){ return false; }
    if(!preg_match($regex_email,$email)){ return false; }
    if(self::getByEmail($email)!=null){ return false; }
    
    $pass = self::passhash($pass);
    
    $query = 'INSERT INTO `User` (`UserTypeID`, `Email`, `Password`, `Name`, `Phone`) '.
             'VALUES ('.$usertype.', "'.$email.'", "'.$pass.'", "'.$name.'", "'.$phone.'");';
    
    return DB::exec($query,true);
    
  }
  
  // password hash
  static function passhash($password, $salt=null){
    $hashstring = $salt.strrev($password);
    return hash('sha256', $hashstring);
  }
  
  // user authentication
  static function auth($email, $password){ 
    $user = self::getByEmail(strtolower($email));
    if(!empty($user)){
      if(self::passhash($password)==strtolower($user['Password'])){
        self::login($user);
        self::$current = $user;
        return true;
      }
    }
    return false;
  }
  
  // login procedure
  static function login(&$user) {
    $loginhash = self::loginhash($user);
    DB::exec('DELETE FROM `Login` WHERE `UserID` = '.$user['UserID'].';');
    $query = 'INSERT INTO `Login` (`UserID`, `LoginHash`) '.
             'VALUES ('.$user['UserID'].', "'.$loginhash.'");';
    DB::exec($query);
    setcookie('login', $loginhash, (time() + 60*60*24*3), '/'); jump('/');
    return true;
  }
  
  // logout procedure
  static function logout($user=null){
    if($user==null && self::logined()) { $user = self::$current; }
    DB::exec('DELETE FROM `Login` WHERE `UserID` = '.$user['UserID'].';');
    setcookie('login',''); jump('/');
    return true;
  }
	
  // current user
  static private $current = null;
  static function current() { return self::$current; }
  
  // is user loged in check
  static private $logined = false;
  static function logined(){
    if(self::$logined) return true;
    else {
      $loginhash = isset($_COOKIE['login']) ? filter($_COOKIE['login']) : null;
      if(!empty($loginhash)){
        $query = 'SELECT `UserID` FROM `Login` WHERE `LoginHash` = "'.$loginhash.'";';
        $result = DB::query($query, false);
        $userid = !empty($result) ? $result['UserID'] : 0;
        if($userid > 0) {
          self::$current = self::getByID($userid);
          self::$logined = true;
          return true;
        }
      }
    }
    return false;
  }
  
  // login hash based on user info, current date and part of passhash
  static function loginhash(&$user) {
    $hashstring = strrev($user['UserID'].$user['Phone'].$user['Email']) . date('my') . strrev(substr($user['Password'],-32));
    return hash('sha256', $hashstring);
  }
  
  
  
}
