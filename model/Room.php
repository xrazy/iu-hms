<?php
class Room {
	
	static function getByID ($id){
		$s = "SELECT * FROM `Room` WHERE `RoomID`='".$id."';";
		$result = DB::query($s, false);
		return $result;
	}
	
	static function getByHotel ($hotel) {
		$s = "SELECT * FROM `Room` WHERE `HotelID`='".$hotel."';";
		$result = DB::query($s);
		return $result;
	}
	
	# all the following getters include city and arrival/departure dates
	
	static function getByLocation ($city, $arrival, $departure){
		$s = "SELECT `pre`.* FROM (SELECT `rm`.* FROM `Hotel` `h`, `Room` `rm`, `Address` `a` ";
		$s.= "WHERE `h`.`HotelID`=`rm`.`HotelID` AND `h`.`AddressID`=`a`.`AddressID` AND `Locality`='".$city."' ";
		$s.= "AND `rm`.`Disabled`='0' AND `h`.`Disabled`='0') AS `pre` OUTER JOIN `Record` AS `rc` ON (`pre`.`RoomID`=`rc`.`RoomID`) ";
		$s.= "WHERE `ArrivalDate`>'".$departure."' OR `DepartureDate`<'".$arrival."';";
		
		$s = "SELECT `rm`.*, `h`.* FROM `Hotel` `h`, `Room` `rm`, `Record` `rc`, `Address` `a` WHERE ";
		$s.= "`h`.`HotelID`=`rm`.`HotelID` AND `rm`.`RoomID`=`rc`.`RoomID` AND `h`.`AddressID`=`a`.`AddressID` AND ";
		$s.= "(`ArrivalDate`>'".$departure."' OR `DepartureDate`<'".$arrival."') AND ";
		$s.= "`Locality`='".$city."' AND  `rc`.`Disabled`='0' AND `rm`.`Disabled`='0' AND `h`.`Disabled`='0'";
		$t = "SELECT `rm`.*, `h`.* FROM `Hotel` `h`, `Room` `rm`, `Address` `a` WHERE ";
		$t.= "`h`.`HotelID`=`rm`.`HotelID` AND `h`.`AddressID`=`a`.`AddressID` AND ";
		$t.= "`Locality`='".$city."' AND `rm`.`Disabled`='0' AND `h`.`Disabled`='0' AND ";
		$t.= "`rm`.`RoomID` NOT IN (SELECT DISTINCT `RoomID` FROM `Record` WHERE `Disabled`='0')";
    
		$query= "(".$s.") UNION DISTINCT (".$t.") ORDER BY `StarRank` DESC, `Price` ASC;";
    
	  $result = DB::query($query);
		
		$query = 'SELECT * FROM `Room` '.
             'INNER JOIN `Hotel` ON (`Room`.`HotelID` = `Hotel`.`HotelID`) '.
             'INNER JOIN `Address` ON (`Hotel`.`AddressID` = `Address`.`AddressID`) '.
             'WHERE `Locality` = "'.$city.'";';
		return $result;
	}
	
	static function getByType ($city, $arrival, $departure, $type){
		$s = "SELECT `rm`.* FROM `Hotel` `h`, `Room` `rm`, `Record` `rc`, `Address` `a` WHERE ";
		$s.= "`h`.`HotelID`=`rm`.`HotelID` AND `rm`.`RoomID`=`rc`.`RoomID` AND `h`.`AddressID`=`a`.`AddressID` AND ";
		$s.= "(`ArrivalDate`>'".$departure."' OR `DepartureDate`<'".$arrival."') AND ";
		$s.= "`Locality`='".$city."' AND `RoomTypeID`='".$type."' AND `Disabled`='0'";
		$t = "SELECT `rm`.* FROM `Hotel` `h`, `Room` `rm`, `Address` `a` WHERE ";
		$t.= "`h`.`HotelID`=`rm`.`HotelID` AND AND `h`.`AddressID`=`a`.`AddressID` AND ";
		$t.= "`Locality`='".$city."' AND `RoomTypeID`='".$type."' AND `Disabled`='0' AND ";
		$t.= "`rm`.`RoomID` NOT IN (SELECT DISTINCT `RoomID` FROM `Record`)";
		$result = DB::query("(".$s.") UNION DISTINCT (".$t.");");
		return $result;
	}
	
	static function getByFeatures ($city, $arrival, $departure, $features){			
		$t = "SELECT `pre`.* FROM ((SELECT `rm`.* FROM `Hotel` `h`, `Room` `rm`, `Record` `rc`, `Address` `a` ";
		$t.= "WHERE `h`.`HotelID`=`rm`.`HotelID` AND `rm`.`RoomID`=`rc`.`RoomID` AND `a`.`AddressID`=`h`.`AddressID` ";
		$t.= "AND (`ArrivalDate`>'".$departure."' OR `DepartureDate`<'".$arrival."') AND `Locality`='".$city."') ";
		$t.= "UNION DISTINCT (SELECT `rm`.* FROM `Hotel` `h`, `Room` `rm`, `Address` `a` WHERE ";
		$t.= "`h`.`HotelID`=`rm`.`HotelID` AND `h`.`AddressID`=`a`.`AddressID` and `rm`.`RoomID` NOT IN ";
		$t.= "(SELECT DISTINCT `RoomID` FROM `Record`))) AS `pre`, `TypeFeature` `tf` WHERE ";
		$t.= "`pre`.`RoomTypeID`=`tf`.`RoomTypeID` AND `FeatureValueID` IN (".$features[0];
		unset($features[0]);
		foreach ($features as $f) {
			$t.= ", ".$f;
		}
		$t.= ") AS `T` AND `pre`.`Disabled`='0' GROUP BY `pre`.`RoomID` HAVING count(*)=";
		$t.= "(SELECT count(*) FROM `T`) ORDER BY PRICE ASC;";
		return DB::query($t);
	}
	
	static function getByPrice ($city, $arrival, $departure, $maxprice){
		$s = "SELECT `rm`.* FROM `Hotel` `h`, `Room` `rm`, `Record` `rc`, `Address` `a` WHERE ";
		$s.= "`h`.`HotelID`=`rm`.`HotelID` AND `rm`.`RoomID`=`rc`.`RoomID` AND `h`.`AddressID`=`a`.`AddressID` AND ";
		$s.= "(`ArrivalDate`>'".$departure."' OR `DepartureDate`<'".$arrival."') AND ";
		$s.= "`Locality`='".$city."' AND `Price`<='".$maxprice."' and `Disabled`='0';";
		$t = "SELECT `rm`.* FROM `Hotel` `h`, `Room` `rm`, `Address` `a` WHERE ";
		$t.= "`h`.`HotelID`=`rm`.`HotelID` AND `h`.`AddressID`=`a`.`AddressID` AND ";
		$t.= "`Locality`='".$city."' AND `Price`<=".$maxprice." AND `Disabled`='0' AND ";
		$t.= "`rm`.`RoomID` NOT IN (SELECT DISTINCT `RoomID` FROM `Record`)";
		$result = DB::query("(".$s.") UNION DISTINCT (".$t.");");
		return $result;
	}
	
	static function getAdvanced ($city, $arrival, $departure, $type, $price, $features){
		$t = "SELECT `pre`.* FROM ((SELECT `rm`.* FROM `Hotel` `h`, `Room` `rm`, `Record` `rc`, `Address` `a` ";
		$t.= "WHERE `h`.`HotelID`=`rm`.`HotelID` AND `rm`.`RoomID`=`rc`.`RoomID` AND `a`.`AddressID`=`h`.`AddressID` ";
		$t.= "AND (`ArrivalDate`>'".$departure."' OR `DepartureDate`<'".$arrival."') AND `Locality`='".$city;
		if (!empty($type)) {
			$t.= "' AND `rm`.`RoomTypeID`='".$type;
		}
		if (!empty($price)) {
			$t.= "' AND `rm`.`Price`<'".$price;
		}
		$t.= "') ";
		$t.= "UNION DISTINCT (SELECT `rm`.* FROM `Hotel` `h`, `Room` `rm`, `Address` `a` WHERE ";
		$t.= "`h`.`HotelID`=`rm`.`HotelID` AND `h`.`AddressID`=`a`.`AddressID` AND ";
		if (!empty($type)) {
			$t.= "`rm`.`RoomTypeID`=".$type." AND ";
		}
		if (!empty($price)) {
			$t.= "`rm`.`Price`=".$price." AND ";
		}
		$t.= " `rm`.`RoomID` NOT IN ";
		$t.= "(SELECT DISTINCT `RoomID` FROM `Record`))) AS `pre`, `TypeFeature` `tf` WHERE ";
		$t.= "`pre`.`RoomTypeID`=`tf`.`RoomTypeID` ";
		if (!empty($features)){
			$t.= "AND `FeatureValueID` IN (".$features[0];
			unset($features[0]);
			foreach ($features as $f) {
				$t.= ", ".$f;
			}
			$t.= ") AS `T`";
		}	
		$t.= " AND `pre`.`Disabled`='0' GROUP BY `pre`.`RoomID`";
		if (!empty ($feature)) {
			$t.= " HAVING count(*)=";
			$t.= "(SELECT count(*) FROM `T`) ";	
		}
		$t.= "ORDER BY PRICE ASC;";
		return DB::query($t);
	}
	
	static function add ($hotel, $number, $price, $type){
		$check = Hotel::getByID($hotel);
		if (empty($check)){
			return -1;
		}
		$check = DB::query ("SELECT * FROM `Room` WHERE `HotelID`='".$hotel."' AND `RoomNum`='".$number."';");
		if (!empty($check)){
			return -1;
		}
		if ($price<=0){
			return -1;
		}
		$s = "INSERT INTO `Room` VALUES ('NULL', '".$hotel."', '".$number;
		$s.= "', '".$price."', '0', '".$type."');";
		$result = DB::exec($s);
		if ($result == 0) {
			return -1;
		}
		else {
			$result = DB::query("SELECT LAST_INSERT_ID();", false);
			return $result['LAST_INSERT_ID()'];
		}
	}
	
	static function changeType ($id, $type){
		$s = "UPDATE `Room` SET `RoomTypeID`='".$type."' WHERE `RoomID`='".$id."';";
		$result = DB::exec($s);
		return $result;
	}
	
	static function changeStatus ($id, $status){
		$s = "UPDATE `Room` SET `Disabled`='".$status."' WHERE `RoomID`='".$id."';";
		$result = DB::exec($s);
		return $result;
	}
	
	static function changePrice ($id, $price){
		if ($price<=0) {
			return 0;
		}
		$s = "UPDATE `Room` SET `Price`='".$price."' WHERE `RoomID`='".$id."';";
		$result = DB::exec($s);
		return $result;
	}
	
	# multiplies all prices of rooms of given type within given hotel by given coefficient
	static function massChangePrice ($hotel, $coefficient, $type){
		if ($coefficient<=0) {
			return;
		}
		$rooms = self::getByHotel($hotel);
		if (empty($rooms)) {
			return;
		} else {
			foreach ($rooms as $room) {
				if ($room['RoomTypeID']==$type) {
					self::changePrice($room['RoomID'], $room['Price']*$coefficient);
				}
			}
		}
		return;
	}
}
