<?php
class Employee {
	
	static function getByID ($id){
		$s = "SELECT * FROM `Employee` `e`, `User` `u` WHERE `e`.`UserID`=`u`.`UserID` AND `e`.`UserID`='".$id."';";
		$result = DB::query($s, false);
		return $result;
	}
	
	static function employ ($typeid, $name, $email, $phone, $hotel, $salary){
		$user = User::getByEmail($email);
		$check = Hotel::getByID($hotel); 
		if (empty($check)){
			return -1;
		}
		if ($salary<=0){
			return -1;
		}
		if (empty($user)){
			$user = User::add($typeid, $email, passHash(generatePassword()), $name, $phone);
		}
		$s = "INSERT INTO `Employee` VALUES ('".$user."', '".$hotel."', '".$salary."', '0');";
		$result=DB::exec($s);
		if ($result==1){
			return $user;
		}
		else {
			return -1;
		}
	}
	
	static function showHotelEmps ($hotel){
		$s = "SELECT * FROM `Employee` `e`, `User` `u` WHERE `e`.`UserID`=`u`.`UserID` AND `HotelID`='".$hotel."';";
		$result = DB::query ($s);
		return $result;
	}
	
	static function showOwnerEmps ($id){
		$s = "SELECT `u`.*, `e`.* FROM `User` `o`, `Hotel` `h`, Employee `e`, User `u` ";
		$s.= "WHERE `o`.`UserID`=`h`.`OwnerID` AND `h`.`HotelID`=`e`.`HotelID` AND `e`.`UserID`=`u`.`UserID` AND ";
		$s.= "`o`.`UserID`='".$id."';";
		$result = DB::query($s);
		return $result;
	}
	
	static function fire ($id){
		$customer = DB::query("SELECT `UserTypeID` FROM `UserType` WHERE `Name`='Customer';");
		DB::exec("UPDATE `Employee` SET `Removed`='1' WHERE `UserID`=".$id.";");
		DB::exec("UPDATE `User` SET `UserTypeID`=".$customer." WHERE `UserID`=".$id.";");
	}
	
	static function salaryChange ($id, $salary){
		if ($salary<=0){
			return 0;
		}
		$s = "UPDATE `Employee` SET `Salary`='".$salary."' WHERE `UserID`='".$id."';";
		$result = DB::exec($s);
		return $result;
	}
	
	# multiplies a salary by a given coefficient for all employees that satisfy a given predicate
	static function salaryMassChange ($hotel, $coefficient, callable $predicate){
		// in: hotel id, coefficient (a number), boolean predicate func on an employee tuple
		if ($coefficient<=0){
			return;
		}
		$employees = self::getByHotel();
		if (empty($employees)){
			return;
		} else {
			foreach ($employees as $emp){
				if (call_user_func_array($predicate, $emp)){
					self::salaryChange($emp['UserID'], $emp['Salary']*$coefficient);
				}
			}
		}
		return;
	}
	
	static function promote ($id, $typeid){
		$s = "UPDATE `User` SET `UserTypeID`='".$typeid."' WHERE `UserID`='".$id."';";
		$result = DB::exec($s);
		return $result;
	}
	
}