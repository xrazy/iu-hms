<?php
class Feature {
	
	#everywhere below, it's gonna be assumed that RoomType ID=0 represents a hotel as a whole instead of a room
	
	static function getByID ($id){
		$s = "SELECT * FROM `Feature` WHERE `FeatureID`='".$id."';";
		$result = DB::query($s, false);
		return $result;
	}
	
	static function getByRoom ($room){
		$s = "SELECT `fv`.`FeatureID` as `nameID`, `fv`.`FeatureValueID` as `valueID`, `f`.`Name` as `name`, `fv`.`Value` as `value` ";
		$s.= "FROM `Room` `r`, `TypeFeature` `tf`, `FeatureValue` `fv`, `Feature` `f` ";
		$s.= "WHERE `r`.`RoomTypeID`=`tf`.`RoomTypeID` AND `r`.`HotelID`=`tf`.`HotelID` ";
		$s.= "AND `tf`.`FeatureValueID`=`fv`.`FeatureValueID` AND `fv`.`FeatureID`=`f`.`FeatureID` AND ";
		$s.= "`r`.`RoomID`=".$room.";";
		return DB::query($s);
	}
	
	static function getByHotel ($hotel){
		$s = "SELECT `fv`.`FeatureID` as `nameID`, `fv`.`FeatureValueID` as `valueID`, `f`.`Name` as `name`, `fv`.`Value` as `value` ";
		$s.= "FROM  `TypeFeature` `tf`, `FeatureValue` `fv`, `Feature` `f` ";
		$s.= "WHERE `tf`.`FeatureValueID`=`fv`.`FeatureValueID` AND `fv`.`FeatureID`=`f`.`FeatureID` AND ";
		$s.= "`tf`.`HotelID`=".$hotel." AND `tf`.`RoomTypeID`=0;";
		return DB::query($s);
	}
	
	static function getValues ($feature){
		return DB::query("SELECT * FROM `FeatureValue` WHERE `FeatureID`=".$feature.";");
	}
	
	# returns all the Feature tuples - i.e. feature categories
	static function getAll (){
		return DB::query("SELECT * FROM `Feature`");
	}
	
	static function add ($hotel, $roomtype, $value){
		$check = Hotel::getByID($hotel);
		if (empty($check)) {
			return 0;
		}
		$check = DB::query ("SELECT * FROM `FeatureValue` WHERE `FeatureValueID`=".$value.";");
		if (empty($check)) {
			return 0;
		}
		$check = "SELECT `FeatureID` FROM `TypeFeature` `tf`, `FeatureValue` `fv` WHERE ";
		$check.= "`tf`.`FeatureValueID`=`fv`.`FeatureValueID` AND `fv`.`FeatureID` IN ";
		$check.= "(SELECT `FeatureID` FROM `tf` WHERE `FeatureValueID`=".$value.") AND ";
		$check.= "`tf`.`HotelID`=".$hotel." AND `tf`.`RoomTypeID`=".$roomtype.";";
		$check = DB::query($check);
		if (!empty($check)) {
			return 0;
		}
		$s = "INSERT INTO `TypeFeature` VALUES (NULL, ".$hotel.", ".$room.", ".$value.");";
		return DB::exec($s);
	}
	
	static function change ($hotel, $roomtype, $value){
		$check = "SELECT `FeatureID` FROM `TypeFeature` `tf`, `FeatureValue` `fv` WHERE ";
		$check.= "`tf`.`FeatureValueID`=`fv`.`FeatureValueID` AND `fv`.`FeatureID` IN ";
		$check.= "(SELECT `FeatureID` FROM `tf` WHERE `FeatureValueID`=".$value.") AND ";
		$check.= "`tf`.`HotelID`=".$hotel." AND `tf`.`RoomTypeID`=".$roomtype.";";
		$check = DB::query ($check);
		if (empty($check)) {
			return self::add($hotel, $roomtype, $value);
		} else {
			$s = "UPDATE `TypeFeature` SET `FeatureValueID`=".$value." WHERE ";
			$s.= "`HotelID`=".$hotel." AND `RoomTypeID`=".$roomtype." AND ";
			$s.= "`FeatureValueID` IN (SELECT `FeatureValueID` from `FeatureValue` ";
			$s.= "WHERE `FeatureID`=".$check[1]['FeatureID'].");";
			return DB::exec($s);
		}
	}
	
	static function remove ($hotel, $roomtype, $feature){
		$s = "DELETE FROM `TypeFeature` WHERE `HotelID`=".$hotel." AND `RoomTypeID`=".$roomtype;
		$s.= " AND `FeatureValueID` IN (SELECT `FeatureValueID` FROM `FeatureValue` WHERE ";
		$s.= "`FeatureID`=".$feature.");";
		return DB::exec($s);
	}
}
